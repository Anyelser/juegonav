{
    "id": "2175b929-e57a-4556-b8db-52f80bfe8f27",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Boton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 0,
    "bbox_right": 184,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "96059936-cce8-4f44-b1a5-4974183a76db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2175b929-e57a-4556-b8db-52f80bfe8f27",
            "compositeImage": {
                "id": "74910fcd-31eb-4be4-8fc3-01e6c009616a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "96059936-cce8-4f44-b1a5-4974183a76db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a5e8e83-5738-4d65-8415-4e737a4d3096",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "96059936-cce8-4f44-b1a5-4974183a76db",
                    "LayerId": "c170a327-1d5a-4406-aeec-62c911e3857b"
                }
            ]
        },
        {
            "id": "38d74b31-1252-4400-9496-3a69ed32ab97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2175b929-e57a-4556-b8db-52f80bfe8f27",
            "compositeImage": {
                "id": "9a1369a9-e015-4c16-a192-7845ddd7703f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "38d74b31-1252-4400-9496-3a69ed32ab97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0610f9aa-b190-493c-b2dd-8a80838104e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "38d74b31-1252-4400-9496-3a69ed32ab97",
                    "LayerId": "c170a327-1d5a-4406-aeec-62c911e3857b"
                }
            ]
        },
        {
            "id": "11fa0834-bfe0-4ae1-8730-5418778efc9c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2175b929-e57a-4556-b8db-52f80bfe8f27",
            "compositeImage": {
                "id": "3a92132b-2b46-4e69-92e8-224e51dd52b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11fa0834-bfe0-4ae1-8730-5418778efc9c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e519016-a98c-4dcf-9e72-d026ef8010ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11fa0834-bfe0-4ae1-8730-5418778efc9c",
                    "LayerId": "c170a327-1d5a-4406-aeec-62c911e3857b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "c170a327-1d5a-4406-aeec-62c911e3857b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2175b929-e57a-4556-b8db-52f80bfe8f27",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 185,
    "xorig": 92,
    "yorig": 21
}