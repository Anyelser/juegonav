{
    "id": "467d69f9-5624-4c6d-b886-144a8982cf55",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_personaje",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "15785cdc-a3c3-402f-bbb2-e71cff35a606",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "467d69f9-5624-4c6d-b886-144a8982cf55",
            "compositeImage": {
                "id": "debd13db-2333-446f-bde3-4f32048ca116",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15785cdc-a3c3-402f-bbb2-e71cff35a606",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa73c0c1-154c-4c15-b877-844691708491",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15785cdc-a3c3-402f-bbb2-e71cff35a606",
                    "LayerId": "a9770a81-88db-47b0-986c-a6a68be81cf4"
                }
            ]
        },
        {
            "id": "15a5e81a-9fda-452f-9a14-30cc5dd5f3b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "467d69f9-5624-4c6d-b886-144a8982cf55",
            "compositeImage": {
                "id": "4909bac7-ec6a-4f57-9a82-6e3c438b3929",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "15a5e81a-9fda-452f-9a14-30cc5dd5f3b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e577ada-26c0-4d69-81eb-fa446f3fe169",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "15a5e81a-9fda-452f-9a14-30cc5dd5f3b8",
                    "LayerId": "a9770a81-88db-47b0-986c-a6a68be81cf4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a9770a81-88db-47b0-986c-a6a68be81cf4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "467d69f9-5624-4c6d-b886-144a8982cf55",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}