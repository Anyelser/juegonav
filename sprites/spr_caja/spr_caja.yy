{
    "id": "9b18dc61-d6fa-492b-acf7-0ab404872c66",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_caja",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f09b9b6b-1d64-4e85-849b-2fb8e9793c3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b18dc61-d6fa-492b-acf7-0ab404872c66",
            "compositeImage": {
                "id": "5afd32f7-4052-4e60-862b-965b9c267041",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f09b9b6b-1d64-4e85-849b-2fb8e9793c3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9ed8f8a6-9b25-4f61-b91b-0d44d277c223",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f09b9b6b-1d64-4e85-849b-2fb8e9793c3b",
                    "LayerId": "bd757c6e-c7c5-4658-a414-0f94d63b6e5a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bd757c6e-c7c5-4658-a414-0f94d63b6e5a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9b18dc61-d6fa-492b-acf7-0ab404872c66",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}