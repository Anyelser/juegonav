{
    "id": "b09a36c5-8c0c-4141-aa12-3925bda61f13",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Back_Fondo",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 599,
    "bbox_left": 0,
    "bbox_right": 599,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aef82a7a-1018-4190-9443-eddfef3935ca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b09a36c5-8c0c-4141-aa12-3925bda61f13",
            "compositeImage": {
                "id": "aa083259-5ee3-46cf-b84f-4059c67df556",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aef82a7a-1018-4190-9443-eddfef3935ca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e12864ef-5d97-4981-aedb-d23cd563ad1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aef82a7a-1018-4190-9443-eddfef3935ca",
                    "LayerId": "5199d591-cc38-4428-88c3-9a489d3b8236"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 600,
    "layers": [
        {
            "id": "5199d591-cc38-4428-88c3-9a489d3b8236",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b09a36c5-8c0c-4141-aa12-3925bda61f13",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 600,
    "xorig": 0,
    "yorig": 0
}