{
    "id": "c9f47eed-b5ba-4974-b185-a9056c7aa731",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "back_Fondo1",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 799,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0e075a97-0e09-477f-888a-9aff5b684bcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9f47eed-b5ba-4974-b185-a9056c7aa731",
            "compositeImage": {
                "id": "5de8f360-41da-449f-ac34-9a5633231b2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e075a97-0e09-477f-888a-9aff5b684bcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9123e0e-da75-47b4-8ea9-42cd67863b4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e075a97-0e09-477f-888a-9aff5b684bcc",
                    "LayerId": "155087cc-d92c-4043-939e-8daeacab46d0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 800,
    "layers": [
        {
            "id": "155087cc-d92c-4043-939e-8daeacab46d0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9f47eed-b5ba-4974-b185-a9056c7aa731",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}