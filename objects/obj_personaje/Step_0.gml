/// @description Dezplazamiento


//con var se crean variables locales
var KeyLeft, KeyRight;

//para la tecla izquierda
KeyLeft  = keyboard_check(vk_left);

//para la tecla derecha
KeyRight = keyboard_check(vk_right);

//velocidad horizontal

if(KeyLeft)
{
    intVX =-1;

}else if(KeyRight)
{
    intVX = 1; 

}


//colision horizontal

repeat(abs(intVX))
{

    if(!place_meeting(x+intVX,y, obj_caja))
    {
        x+=intVX;
    
    
    }else{
        intVX=0;
        break;
    }

}

