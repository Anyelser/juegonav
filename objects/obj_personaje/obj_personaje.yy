{
    "id": "1d53d87d-e75c-4aab-8deb-6e55747cd539",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_personaje",
    "eventList": [
        {
            "id": "59e2bf7d-3c83-4cef-b105-6023d4a78377",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1d53d87d-e75c-4aab-8deb-6e55747cd539"
        },
        {
            "id": "7f5d1455-f5f6-46e8-b06a-181612cc4f9d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1d53d87d-e75c-4aab-8deb-6e55747cd539"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "467d69f9-5624-4c6d-b886-144a8982cf55",
    "visible": true
}