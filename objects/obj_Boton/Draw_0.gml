/// @description PARA DIBUJAR EN PANTALLA
//para game maker studio anterior al 8
draw_self();

//para game mamer sudio 8
//draw_sprite(sprite_index,image_index,x,y)
      
//PARA ESTABLECER UN TIPO DE FUENTE  
draw_set_font(fnt_texto);

//para establecer el color 
draw_set_color(c_white);

//alineacion
draw_set_halign(fa_center);
draw_set_valign(fa_middle);
draw_text(x,y,string_hash_to_newline(texto));

