{
    "id": "b1a2cde6-4ba2-486d-b66b-d6436343349f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Boton",
    "eventList": [
        {
            "id": "dc4cbc02-7535-405c-8890-b303735481bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b1a2cde6-4ba2-486d-b66b-d6436343349f"
        },
        {
            "id": "0984a519-8bbf-4036-b756-efd8e5503fae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "b1a2cde6-4ba2-486d-b66b-d6436343349f"
        },
        {
            "id": "71a4d7df-d26d-4586-82c1-057514891af4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "b1a2cde6-4ba2-486d-b66b-d6436343349f"
        },
        {
            "id": "a03ccaee-662e-4b98-8554-cf8d9711cdd2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "b1a2cde6-4ba2-486d-b66b-d6436343349f"
        },
        {
            "id": "b6796562-3618-49ad-b0fd-1b7068df3414",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "b1a2cde6-4ba2-486d-b66b-d6436343349f"
        },
        {
            "id": "95ec325a-4edb-4a52-a863-0ed9c8386ddc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b1a2cde6-4ba2-486d-b66b-d6436343349f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 0,
    "physicsShapePoints": [
        
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2175b929-e57a-4556-b8db-52f80bfe8f27",
    "visible": true
}