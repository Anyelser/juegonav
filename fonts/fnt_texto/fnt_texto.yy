{
    "id": "1e7ec0bb-7836-43d8-96a2-c7a173710c7a",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_texto",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 1,
    "first": 0,
    "fontName": "Bahnschrift SemiBold",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 113,
            "Value": {
                "id": "4f0d2be7-21f7-4432-a33c-10a0c9335383",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 167,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "75bbfd87-f243-4086-8f2f-51c28cad7180",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 17,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "ea31584b-1f9a-49b7-a5fb-361d1bc086e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 57,
                "y": 48
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "40d95599-c1a8-440f-9d4a-7cf1a62a6a81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 17,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 57,
                "y": 70
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "6b5b7670-8f03-4da8-940e-77f2757151d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 14,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 70
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "2e88975e-00a2-47a9-98fe-ff407288ed98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 17,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "62682a12-8d67-4c57-86d2-20b36cb1b5d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 111,
                "y": 48
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "82893e28-f574-48c5-a292-b992ee1a0fe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 149,
                "y": 25
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "803f0d8e-bada-459f-9361-ee07b5f05115",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 30,
                "y": 48
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "ec883341-1222-4910-9aba-f3118e3662dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 57,
                "y": 25
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "0892940e-84a5-4ee4-bbb4-7d85e86c9ead",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 120,
                "y": 70
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "89a65659-13ee-4896-9f6b-4ece51aefb1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 8,
                "x": 110,
                "y": 25
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "38f42268-ae67-4dfd-ba42-8f5f2bb03216",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "06096b18-b91e-4db5-a60f-fffd1e57ea30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "d720141a-4160-493a-9a18-30fc2131d749",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 20,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 189,
                "y": 48
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "718d0278-8416-4d7a-af51-0e78c4d326e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 75,
                "y": 48
            }
        },
        {
            "Key": 32,
            "Value": {
                "id": "8f50669d-ee47-48f7-825b-d54226957c03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 45,
                "y": 70
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "a770eb9d-b721-4718-8867-8550800b29bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 39,
                "y": 70
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "b92f359b-74c8-47c5-be16-33bd0844a378",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 68,
                "y": 25
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "746a893d-3000-47ae-b8a2-4a42ff177bec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 138,
                "y": 48
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "7fb5b8de-2b45-4aa9-9a22-03fe63e05894",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 17,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 174,
                "y": 48
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "7827c05b-2c97-4aab-ac2a-7f53dc81f2ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 39,
                "y": 48
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "4c339599-60df-4083-adc7-66106e14aa77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 209,
                "y": 25
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "a267876d-f01f-4d3a-be39-a6dbb0762027",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 147,
                "y": 48
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "6561b457-9ee1-4f7d-afe0-ef681fd2d0a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 103,
                "y": 70
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "fe49780b-1ea9-4759-a7e7-2ee004a6fd4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "ea69cf73-da01-4a38-a851-71a549dc1e44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 219,
                "y": 25
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "60aa8f0d-a283-4abd-82e3-e2ed568077a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 78,
                "y": 70
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "2be1313d-bc92-41ac-bd74-db3e032c751a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "2fa02faf-3ffd-4b33-b881-31c6f5990b8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 48
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "a88772ce-03cd-49e4-8161-ee6b848341f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": -1,
                "shift": 6,
                "w": 8,
                "x": 100,
                "y": 25
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "981fa13f-5835-4601-b558-7aebeee0b3da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 221,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f6d62b70-e2fd-4e10-b4e4-ce9bcfac3298",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 90,
                "y": 25
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "844c94c2-ef07-4d17-adc7-01779ec40b33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 99,
                "y": 2
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "abdb725b-bb66-4b36-a25d-23d687a42fec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 20,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "78ece29f-bbbc-4733-bd91-c8e5988907e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "08b60b55-0086-4c81-a162-4ece87a0caef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 120,
                "y": 48
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "6936649c-c67b-4432-8446-ef6fa5a408f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 239,
                "y": 25
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "eeb94583-2a5f-4243-be93-59c88c0246c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 188,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "8cd89ae8-d827-4a76-99f5-fac554a3c65a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 199,
                "y": 2
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "eb0aa083-5250-4056-825b-5c8391fa046f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 203,
                "y": 48
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "affcb527-b826-4332-8f51-c8a5ee46e48b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 120,
                "y": 25
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "1f3e08dd-b1af-49f8-b90a-819798116e6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 15,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 165,
                "y": 48
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "967af983-d791-4656-a9ab-2da8fae41b66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 8,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 124,
                "y": 70
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "3673d5a8-b410-4906-8f45-6add314fff01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 93,
                "y": 48
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "4d6227e3-04f8-4739-a794-a565d325212b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 159,
                "y": 25
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "1dd3d0aa-c9ee-4377-b17d-7fab6670569f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 129,
                "y": 25
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "f2c5360f-09ab-4dbb-b29a-ae1875eecdf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 13,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 210,
                "y": 48
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "ee059377-96d0-48fc-a7db-7f3f7536ae44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 17,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 98,
                "y": 70
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "258b6647-43af-40c1-9a4e-e2fd6c4f3fc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 199,
                "y": 25
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "dd885978-196d-4cb8-b4ac-e4b061119770",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 17,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 93,
                "y": 70
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d76acce7-74ff-4727-be71-4ea5310d8366",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 20,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 18,
                "y": 70
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "cd71a771-eba5-4680-8969-7e172ed8690c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 25
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "695af406-ace1-4341-87ab-29cde2c4f4bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 17,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 235,
                "y": 48
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "746599bc-3d95-4669-8d9f-cb5d23465337",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 17,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "a92c2b12-fed5-4925-a830-f7cbcce61448",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 179,
                "y": 25
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "309cac82-2d2d-4105-b9b1-0b130dd6ddd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 79,
                "y": 25
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "5beb80b1-9524-4b91-8cc3-c090129e76ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 169,
                "y": 25
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "faa9995a-e1a6-4749-acb3-6d613def3dbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "9b3a98c9-7a90-41ab-8554-3f9c4d5463f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 21,
                "y": 48
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "db5b34d1-9efa-4867-bfe5-4e19e24d53a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 24,
                "y": 25
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "0deb2d8f-3912-4ccc-8c49-fab493fef6ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 9,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 130,
                "y": 70
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "6c45ae70-9b5f-4a40-becb-d0daa2c22cc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 9,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 69,
                "y": 70
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "c74568f4-04a6-4b18-b7fe-6d2ccb9b9db2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 20,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 83,
                "y": 70
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "e6aa3878-76b7-4b62-9989-b4d874884c14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 17,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 63,
                "y": 70
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "0519d14e-e9d9-4092-877e-4689313235af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 66,
                "y": 48
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "60f64e6e-2c8d-4d6f-b53b-418831ba38c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 84,
                "y": 48
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "74323362-3d90-4e9b-891d-fb95702232b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 46,
                "y": 25
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "1aef46fe-dd4f-473b-9a39-2cc1d0d3140e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 15,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 227,
                "y": 48
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "78547487-c03e-43ef-96fb-9234c616bc5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 35,
                "y": 25
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "b5ebc7df-d798-4195-9ee6-b80a5876ad88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 12,
                "y": 48
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "1fcb009e-3b49-4355-b37a-aa09b45d5392",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 17,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 51,
                "y": 70
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "5518e37c-2a5a-4c3b-aeed-c2febedfb209",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "c5b19ea7-929d-49ad-ba98-e2dcf1d9a587",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 196,
                "y": 48
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "fc26dd7b-01ed-4d2e-a21a-3957abdfe521",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "e0bdc246-997a-464b-b6f2-18cb6f017eeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 156,
                "y": 48
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "84fe6a51-fb67-43b5-937e-d7a0b901cda4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 20,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 182,
                "y": 48
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "e0c9afc3-3f5d-421a-bd7d-5560ed3be677",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 12,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 242,
                "y": 48
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "b58ce393-d303-4f2a-9321-2d6707528191",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 129,
                "y": 48
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "9f2f700f-adb1-43ad-b8a6-1f607b495651",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 102,
                "y": 48
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "30977ff3-b86f-4173-859c-b9f7f7b5398c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 15,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 219,
                "y": 48
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "e99daefb-9623-4b5f-a3b1-5e7f728833e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 14,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 10,
                "y": 70
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "6d194d58-d868-407e-9927-6c1e4430b468",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 11,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 24,
                "y": 70
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "6b911f8d-ab49-4a03-912b-3d3b7806f185",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 139,
                "y": 25
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "371d49a9-6169-4bef-9bf3-6ebdcf6e9807",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 33,
                "y": 70
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "c3e94aa2-3f99-4357-9e96-d98a5b464a8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 157,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "bad0428a-954f-48d8-bbba-a16023a76fe8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 243,
                "y": 2
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "d4028c5f-c369-4633-8e2a-5a11760abdaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 229,
                "y": 25
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "142c5006-2406-4aa6-bf2f-26a71366a8d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 189,
                "y": 25
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "893262d7-3fc1-48af-baca-12ea8f059f6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 48,
                "y": 48
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "f26c9a48-1e05-4f11-bc58-4ba29e6d7ae4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 18,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 88,
                "y": 70
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "9bead3e4-49b3-42e5-a9e3-4b22367bbd33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 9,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 113,
                "y": 70
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "b98bb40e-4780-44af-a3b9-1d51465a5b5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "69fd4555-b2b9-4c1f-a61f-97d79aa4b2fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 123,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "7773d171-cf6e-4a49-a0f2-25c54fb9cd57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 17,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 13,
                "y": 25
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "658243e4-125c-49a9-a122-79a6cf147342",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 108,
                "y": 70
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": true,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}